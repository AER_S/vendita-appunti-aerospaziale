# Aerospaziale
Ciao!
Questa è la repository dedicata alla Laurea Triennale e Magistrale di Ingegneria Aerospaziale (Polimi) gestita dal [PoliNetwork](https://polinetwork.github.io/it/index.html)

Vi troverete i contatti di studenti del corso che vendono gli appunti per i vari i corsi.
PoliNetwork non si assume nessuna responsabilità riguardante il contenuto venduto dai singoli ne delle modalità con cui gestiranno il tutto.

Per essere aggiunti all'elenco scrivete a https://t.me/Raif9
(sito privo di commissioni)




 -----------------

Hello everyone this is the repository of BSc and MSc in Aerospace Engineering (Polimi) managed by [PoliNetwork](https://polinetwork.github.io/it/index.html)

Here you will find contacts of students of the course who want to sell their notes.
PoliNetwork does not assume any responsibility regarding the content sold by the individual nor of the way in which they will manage everything.

To be added to the list write to https://t.me/Raif9
(site without fees)


**ATTENTION**: You mustn't upload any copyright protected or in any other author protection form cover file on this repository or any of the ones associated to it.
